function writeScreen(value){
    
    //getelementById señala una "id" del html
    //innerHTML captura el contenido de lo que hay dentro de la etiqueta html con esa id <p id="entryNumber"> LO QUE HAY DENTRO </p>
    var operando = document.getElementById("entryNumber").innerHTML;
    
    if(operando == ''){
        document.getElementById("entryNumber").innerHTML = value;
    }
    if(operando == '.' && value == '.'){
        return false
    }
    if(operando == '0' && value != '.'){
        return false
    }
    else{
        //Concatenamos con +=  , esto significa que "operando" añade a sí mismo el valor de "value";
        operando+=value;

        if(Number.isInteger(value) || value == "."){
            document.getElementById("entryNumber").innerHTML=operando;
        }
        else{
            document.getElementById("exitNumber").innerHTML=operando;
            document.getElementById("entryNumber").innerHTML = "";
        }       
    }   
}

function operation(){

   var operacion = document.getElementById("exitNumber").innerHTML + document.getElementById("entryNumber").innerHTML;
   document.getElementById("entryNumber").innerHTML=eval(operacion)
   document.getElementById("exitNumber").innerHTML = "";

}

function reset(){
    var reset= document.getElementById("entryNumber").innerHTML = "";
    
}

function allclear(){
    var allclear = document.getElementById("entryNumber").innerHTML = "";
     document.getElementById("exitNumber").innerHTML = "";

}

function retro(){
    
    var str = document.getElementById("entryNumber").innerHTML
    var res = str.slice(0, -1);

    document.getElementById("entryNumber").innerHTML = res
}
